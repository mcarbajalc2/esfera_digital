<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\MailMCC;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function send(Request $request){
    	$mailData = new \stdClass();
    	$mailData->nombre = $request->input('nombre');
    	$mailData->telefono = $request->input('telefono');
    	$mailData->pais = $request->input('pais');
    	$mailData->mensaje = $request->input('mensaje');
    	$asd = Mail::to('ivan@esfera.pe')->send(new MailMCC($mailData));
    }
}
