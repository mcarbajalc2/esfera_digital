<!DOCTYPE html>
<html lang="es"">
<head>
	<meta charset="UTF-8">
	<title>Esfera Digital</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
</head>
<body>
	<header>
		<nav class="container d-flex flex-wrap p-0 flex-md-nowrap">
			<div class="container d-flex">
				<a class="d-block">
					<img src="{{asset('images/logo.png')}}">
				</a>
				<button id="btn-menu" class="btn-menu mr-0 ml-auto d-md-none">
					<label></label>
					<label></label>
					<label></label>
				</button>
			</div>
			<div class="mr-0 ml-auto d-block d-md-block list-container col-12 col-md-auto p-0 menu-container">
				<ul class="d-flex flex-column flex-md-row list-unstyled p-0 mt-3 mt-md-0">
					<li><a class="d-block" href="#" id="experiencia-btn">NUESTRA EXPERIENCIA</a></li>
					<li><a class="d-block" href="#" id="comercio-btn">¿QUÉ HACEMOS?</a></li>
					<li><a class="d-block" href="#" id="contacto-btn">CONTACTO</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<section class="banner d-flex align-items-center justify-content-center">
		<div class="caption text-center pt-5 mt-5">
			<h1 class="mb-5">CREAMOS EXPERIENCIAS<br>DIGITALES</h1>
			<a href="#" class="btn btn-primary" id="vermas-btn">VER MÁS</a>
		</div>
	</section>
	<section class="experiencia ">
		<div class="container text-center">
			<h2 class="mb-4">Nuestra <span class="o">Experiencia</span></h2>
			<p>Contamos con más de 20 años desarrollando estrategias de comunicación interactiva. Hemos creado más de 300 Web Sites, más de 50 Sistemas de Comercio electrónico para empresas pequeñas, medianas y grandes, hemos desarrollado más de 50 sistemas de información, hemos asesorado empresas en proyectos de comunicación interactiva en más de 40 oportunidades, contamos con clientes en Sudamérica, Norteamérica y la Unión Europea.</p>
		</div>
	</section>
	<section class="comercio-electronico">
		<div class="col-12">
			<div class="row">
				<div class="col-12 col-md-6 main-img">
					<img src="{{asset('images/foto1.png')}}">
				</div>
				<div class="col-12 col-md-6 d-flex flex-column justify-content-center">
					<h3>Comercio <span class="o">Electrónico</span></h3>
					<ul>
						<li><span>Más de 15 años de experiencia en E-commerce, hemos creado tiendas virtuales para empresas pequeñas, medianas y grandes.</span></li>
						<li><span>Somos una de las agencias con mayor experiencia en desarrollo de Tiendas Virtuales en Sudamérica.</span></li>
						<li><span>Nuestras tiendas tienen la capacidad de integrarse a los sistemas contable - administrativo de las empresas y con proveedores de servicios logísticos.</span></li>
						<li><span>Hemos creado sistemas de e-commerce a clientes de todo el mundo, integrado a todo tipo de pasarelas de pago.</span></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="servicios-digitales">
		<div class="col-12">
			<div class="row d-flex flex-column-reverse flex-md-row">
				<div class="col-12 col-md-6 d-flex flex-column justify-content-center bg-inverse">
					<h3>Diseño de Servicios Digitales</h3>
					<ul>
						<li>Creamos Páginas web, Apps y todo tipo de sistemas basados en tecnología digital.</li>
						<li>Nuestro proceso de desarrollo ha sido afinado por más de 20 años, durante este periodo hemos obtenido más del 99% de clientes satisfechos, como lo logramos? a través de la mejora continua de nuestros procesos, nuestra metodología hoy en día es a prueba de balas!</li>
					</ul>
				</div>
				<div class="col-12 col-md-6 main-img">
					<img src="{{ asset('images/foto2.png') }}">
				</div>
			</div>
		</div>
	</section>
	<section class="transformacion-y-gestion-digital">
		<div class="col-12">
			<div class="row">
				<div class="col-12 col-md-6 main-img">
					<img src="{{asset('images/foto3.png')}}">
				</div>
				<div class="col-12 col-md-6 d-flex flex-column justify-content-center">
					<h3>Transformación & <span class="o">Gestión Digital</span></h3>
					<ul>
						<li><span>Tenemos 20 años transformando empresas para el mundo digital.</span></li>
						<li><span>Nuestros expertos estudian la tecnología y nuevos métodos de forma permanente.</span></li>
						<li>
							<span>Nuestro método de transformación digital se basa en 4 pilares:</span>
							<ul class="list-style-1 list-unstyled p-0 pt-4">
								<li><span class="o">PERSONAS: </span><span>Lograr una mejor experiencia mediante tecnología digital</span></li>
								<li><span class="o">INNOVACIÓN: </span><span>Aplicada y práctica para transformar rápidamente</span></li>
								<li><span class="o">PROCESOS: </span><span>Optimizar y monitorear la ejecución en tiempo real</span></li>
								<li><span class="o">INFORMACIÓN: </span><span>Incorporar herramientas de gestión por indicadores</span></li>
							</ul>
						</li>
					</ul>					
				</div>
			</div>
		</div>
	</section>
	<section class="clientes">
		<div class="container">
			<h2 class="text-center mb-5">Nuestros <span class="o">Clientes</span></h2>			
			<div class="slider-container">
				<label class="slide-btn btn-left d-none d-md-none">
					<span class="icon-chevron-left"></span>
				</label>
				<label class="slide-btn btn-right d-none d-md-none">
					<span class="icon-chevron-right"></span>
				</label>		
				<div class="slide-1 col-12">
					<div class="row">
						<div class="col-6 col-md-3">
							<img src="{{asset('images/c1.png')}}">
							<img src="{{asset('images/c2.png')}}">
							<img src="{{asset('images/c3.png')}}">
						</div>
						<div class="col-6 col-md-3">
							<img src="{{asset('images/c4.png')}}">
							<img src="{{asset('images/c5.png')}}">
							<img src="{{asset('images/c6.png')}}">
						</div>
						<div class="col-6 col-md-3">
							<img src="{{asset('images/c7.png')}}">
							<img src="{{asset('images/c8.png')}}">
							<img src="{{asset('images/c9.png')}}">
						</div>
						<div class="col-6 col-md-3">
							<img src="{{asset('images/c10.png')}}">
							<img src="{{asset('images/c11.png')}}">
							<img src="{{asset('images/c12.png')}}">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="contactanos bg-inverse">
		<div class="col-12 text-center d-flex flex-column align-items-center">
			<h2 class="mb-5">Escríbenos</h2>
			<form class="col-12">
				<div class="alert alert-success d-none" role="alert">
				  Formulario enviado.
				</div>
				<div class="alert alert-danger d-none" role="alert">
				  Error al enviar formulario.
				</div>
				<input type="text" class="form-control col-12" placeholder="Nombre y Apellidos" name="nombre">
				<input type="text" class="form-control col-12" placeholder="Teléfono" name="telefono">
				<input type="text" class="form-control col-12" placeholder="País" name="pais">
				<textarea name="mensaje" id="" cols="30" rows="4" class="form-control col-12 mb-5" placeholder="Comentario"></textarea>
				<button class="btn btn-secondary" type="submit">Enviar</button>
			</form>
		</div>
	</section>
	<footer>
		<div class="col-12 text-center pt-4 pb-4">
			<small class="m-0">Todos los derechos reservados, Esfera Digital 2018</small>
		</div>
	</footer>
	<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
</body>
</html>