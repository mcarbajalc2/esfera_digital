$(document).ready(function(e){

	$('#experiencia-btn').click(function(event) {
		event.preventDefault();
		$('.experiencia')[0].scrollIntoView({
			behavior: "smooth",
    		block: "start"
		});
		$(".menu-container").removeClass('active');
	});

	$('#comercio-btn').click(function(event) {
		event.preventDefault();
		$('.comercio-electronico')[0].scrollIntoView({
			behavior: "smooth",
			block: "start"
		});
		$(".menu-container").removeClass('active');
	});

	$('#contacto-btn').click(function(event) {
		event.preventDefault();
		$('.contactanos')[0].scrollIntoView({
			behavior: "smooth",
			block: "start"
		});
		$(".menu-container").removeClass('active');
	});

	$('#vermas-btn').click(function(event){
		event.preventDefault();
		$(".experiencia")[0].scrollIntoView({
			behavior: "smooth",
			block: "start"
		});
		$(".menu-container").removeClass('active');
	});

	$('.contactanos form').submit(function(event) {
		event.preventDefault();
		var nombre = event.target.nombre.value;
		var telefono = event.target.telefono.value;
		var pais = event.target.pais.value;
		var mensaje = event.target.mensaje.value;
		
		$.ajax({
			url: '/sendMail',
			type: 'POST',
			dataType: 'html',
			data: {nombre: nombre, telefono: telefono, pais: pais, mensaje: mensaje},
		})
		.done(function() {
			$(".contactanos .alert-success").removeClass('d-none');
			setTimeout(function(){
				$(".contactanos .alert-success").addClass('d-none');
			},5000);
			console.log("success");
		})
		.fail(function() {
			$(".contactanos .alert-danger").removeClass('d-none');
			setTimeout(function(){
				$(".contactanos .alert-danger").addClass('d-none');
			},5000);
			console.log("error");
		})
		
	});

	$("#btn-menu").click(function(event) {
		if($(".menu-container").hasClass('active')){
			$(".menu-container").removeClass('active');
		}else{
			$(".menu-container").addClass('active');
		}
	});
});